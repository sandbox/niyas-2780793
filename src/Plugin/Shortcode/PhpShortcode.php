<?php
/**
 * @file
 * Wraps your content with a div with bootstrap column size classes.
 */

namespace Drupal\php_shortcode\Plugin\Shortcode;

use Drupal\Core\Language\Language;
use Drupal\shortcode\Plugin\ShortcodeBase;

/**
 * Provides a shortcode for executing php scripts.
 *
 * @Shortcode(
 *   id = "php",
 *   title = @Translation("Php code"),
 *   description = @Translation("Executes php scripts")
 * )
 */
class PhpShortcode extends ShortcodeBase {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
	ob_start();
	print eval(strip_tags($text));
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = array();
    $output[] = '<p><strong>' . t('[php]Your php script here[/php]') . '</strong> ';
    if ($long) {
      $output[] = t('Executes php script given inside the shortcode') . '</p>';
    }
    else {
      $output[] = t('Executes php script') . '</p>';
    }

    return implode(' ', $output);
  }
}